# -*- coding: utf-8 -*-
"""
Created on Thu Dec 17 01:40:09 2020

@author: Miguel

hero sprites from Wulax on opengameart.org
tiles and enemy sprites from Buch on opengameart.org
"""

import sys, pygame, random, math, threading

def gameOver():
    pygame.quit()
    sys.exit()
   
numTiles = 0        #track the number of tiles
tiles = []          #hold the tiles in a list so they can be easily accessed
openings = True     #will be false when there are no more open edges on existing tiles
numDungeon = 0      #holds the number of dungeons the player has been to
boss = 0            #holds the boss sprite
firstTime = 3       #used in dungeon generation
#colors:
black = 0, 0, 0
white = 255, 255, 255
navyBlue = 0, 0, 128


#side order is [left, top, right, bottom] (starts at left and goes clockwise)
#we need 2 different lists that hold the open tile sides, 
#openSides is for player movement, genSides for use in dungeon generation
class tile:
    def __init__(self, imgPath, sides = [1,1,1,1], b = False):
        self.img = pygame.image.load(imgPath)
        self.rect = self.img.get_rect()
        self.openSides = sides[:]
        self.genSides = sides[:]
        self.battle = b

#for various images used in battle display
class imgObject:
    def __init__(self, imag, file = True):
        if file:
            self.img = pygame.image.load(imag)
        else:
            self.img = imag
        self.rect = self.img.get_rect()
    def setRect(self):
        self.rect = self.img.get_rect()
        
#class for holding stat blocks, usually used as a nested class
#optional n for starting a stat block with a higher base value
class statBlock:
    def __init__(self, n = 1, lvl = 1):
        self.con = n
        self.str = n
        self.dex = n
        self.int = n
        self.wis = n
        self.lvl = lvl
        self.maxHP = 10 * (self.lvl + self.con)
        self.HP = self.maxHP
        self.dodge = 10 * math.sqrt(self.dex)
        self.physHit = 10 * math.sqrt(self.dex)
        self.magHit = 10 * math.sqrt(self.wis)
        self.blocked = 0
        self.barrier = 0
        self.burn = 0
        self.stun = False
        self.freeze = 0
    def resetHP(self):
        self.maxHP = 10 * (self.lvl + self.con)
        self.HP = self.maxHP
    def resetDefenses(self):
        self.dodge = 10 * math.sqrt(self.dex)
        self.blocked = 0
        self.barrier = 0
        
#enemy class, generates an enemy for battle
class enemy:
    def __init__(self, n = 1):
        #self.img = 
        #self.rect = 
        self.stats = statBlock(n, hero.stats.lvl)
        points = int(((numDungeon * numDungeon) / 3) + self.stats.lvl + 
            random.randint(-(self.stats.lvl), 0) + random.randint(0,n))
        while points > 0:
            r = random.randint(0,4)
            if r == 0:
                self.stats.con += 1
            elif r == 1:
                self.stats.str += 1
            elif r == 2:
                self.stats.dex += 1
            elif r == 3:
                self.stats.int += 1
            elif r == 4:
                self.stats.wis += 1
            points -= 1
        #if the enemy has only one stat that is its highest, it will be a specific type
        #and that stat will be increased, otherwise it will be a slime
        statList = [self.stats.con, self.stats.str, self.stats.dex, self.stats.int, self.stats.wis]
        numMax = 0
        for i in range(5):
            if statList[i] == max(statList):
                numMax += 1
        if numMax > 1:
            self.name = 'Slime'
            if max(statList) > 1:
                if self.stats.con == max(statList):
                    self.stats.con -= 1
                if self.stats.str == max(statList):
                    self.stats.str -= 1
                if self.stats.dex == max(statList):
                    self.stats.dex -= 1
                if self.stats.int == max(statList):
                    self.stats.int -= 1
                if self.stats.wis == max(statList):
                    self.stats.wis -= 1
        elif self.stats.con == max(statList):
            self.name = 'Troll'
            self.stats.con += 1
        elif self.stats.str == max(statList):
            self.name = 'Ogre'
            self.stats.str += 1
        elif self.stats.dex == max(statList):
            self.name = 'Goblin'
            self.stats.dex += 1
            self.stats.dodge += self.stats.dex
        elif self.stats.int == max(statList):
            self.name = 'Fiend'
            self.stats.int += 1
        elif self.stats.wis == max(statList):
            self.name = 'Wraith'
            self.stats.wis += 1
        self.stats.resetHP()
        self.img = pygame.image.load('images\\' + self.name + '.png')
        self.rect = self.img.get_rect()
                
#Player character's class
class player:
    def __init__(self,x,y, name = "Player"):
        #save the x and y for later so you don't have to keep passing it
        self.x = x
        self.y = y
        self.xp = 0
        self.name = name
        self.img = pygame.image.load("images\heroDown1.png")
        self.rect = self.img.get_rect()
        self.rect.move_ip(x,y)
        #imageSwitch is for going through running motions, it holds the 
        #direction player is facing, which image of a given direction set to use,
        #and the number of frames spent on the current image
        self.imgSwitch = [0,0,0]
        self.stats = statBlock(1)
        self.statpoints = 10
        
    #sets the image based on the imgSwitch information
    def setImage(self):
        #since we call this regularly, we use it to check our xp for a level-up
        leveledUp = False
        if self.xp >= ((self.stats.lvl * 10)):
            self.xp = 0
            self.statpoints += 3
            self.stats.lvl += 1
            levelUp(hero = self)
            leveledUp = True
            
        if self.imgSwitch[2] >= 3:
            self.imgSwitch[2] = 0
            self.imgSwitch[1] = (self.imgSwitch[1] + 1) % 4
            #we also use it to drive  our out of combat hp regen
            self.stats.HP += (math.sqrt(self.stats.con * self.stats.wis) / 10) + 1
            if self.stats.HP > self.stats.maxHP:
                self.stats.HP = self.stats.maxHP
        imgName = 'images\hero'
        if self.imgSwitch[0] == 0:
            imgName += 'Left'
        elif self.imgSwitch[0] == 1:
            imgName += 'Up'
        elif self.imgSwitch[0] == 2:
            imgName += 'Right'
        elif self.imgSwitch[0] == 3:
            imgName += 'Down'
        imgName += str(self.imgSwitch[1] + 1)
        imgName += '.png'
        self.img = pygame.image.load(imgName)
        self.rect = self.img.get_rect()
        self.rect.move_ip(self.x, self.y)
        return(leveledUp)
        
#intro screen
def introScreen():
    global width
    global height
    global screen
    #s will be black and placed in front of the image, with its alpha adjusted
    #to give the look of the logo fading in and out
    s = pygame.Surface((width, height))
    
    rosiness = imgObject("images/logo_rosiness__blackSwan.jpg")
    rosiness.img = pygame.transform.scale(rosiness.img, (int(((height * rosiness.rect.width) / rosiness.rect.height)), int(height)))
    rosiness.setRect()
    rosiness.rect.move_ip((width/2 - rosiness.rect.width/2), 0)
    
    s.fill(black)
    screen.fill(black)
    screen.blit(rosiness.img, rosiness.rect)
    #fade in by gradually making s transparent
    for i in range(62):
        clock.tick(30)
        s.set_alpha(255 - (4 * i))
        screen.blit(rosiness.img, rosiness.rect)
        screen.blit(s, s.get_rect())
        pygame.display.flip()
        #pump events so windows doesn't think the program's not responding
        pygame.event.pump()
    #fade out by gradually making s opaque  
    for i in range(31):
        clock.tick(30)
        s.set_alpha(8 * i)
        screen.blit(rosiness.img, rosiness.rect)
        screen.blit(s, s.get_rect())
        pygame.display.flip()
        #pump events so windows doesn't think the program's not responding
        pygame.event.pump()
    #delete the things used only in this function, so they don't sit in memory
    del s
    del rosiness.rect
    del rosiness.img
    del rosiness 
    
#character naming screen
def nameCharacter():
    global hero
    global screen
    confirm = False
    #put the alphabet in indexed lists to make them easier to use
    alphabet = [['a','b','c','d','e','f','g','h','i','j','k','l','m'],['n','o','p','q','r','s','t','u','v','w','x','y','z'],
                ['A','B','C','D','E','F','G','H','I','J','K','L','M'],['N','O','P','Q','R','S','T','U','V','W','X','Y','Z']]
    textObject = [[0 for i in range(13)],[0 for j in range(13)],[0 for k in range(13)],[0 for l in range(13)]]
    
    #create the boxes used for page formatting
    botBox = imgObject("images/RoundedBox.png")
    botBox.img = pygame.transform.scale(botBox.img, (int (width), int (height/2)))
    botBox.setRect()
    botBox.rect.move_ip(0, height - botBox.rect.height)
    
    midBox = imgObject("images/RoundedBox.png")
    midBox.img = pygame.transform.scale(midBox.img, (int (width), int (height/5)))
    midBox.setRect()
    midBox.rect.move_ip(0, (height*3)/10)
    
    topBox = imgObject("images/RoundedBox.png")
    topBox.img = pygame.transform.scale(topBox.img, (int (width), int (height * 3 / 10)))
    topBox.setRect()
    
    #create the objects for the alphabet buttons
    for i in range(4):
        for j in range(13):
            textObject[i][j] = imgObject(font.render(alphabet[i][j], True, black), False)
            textObject[i][j].img = pygame.transform.scale(textObject[i][j].img, 
                                    (int((textObject[i][j].rect.width * botBox.rect.height) 
                                    / (5 * textObject[i][j].rect.height)), int(botBox.rect.height/5)))
            textObject[i][j].setRect()
            textObject[i][j].rect.width = botBox.rect.height/5
            textObject[i][j].rect.move_ip((j * textObject[i][j].rect.width) + botBox.rect.height/10, height - 
                                     (((4 - i) * textObject[i][j].rect.height) + botBox.rect.height/10))
    #confirm button      
    confirmObject = imgObject(font.render('Confirm', True, black), False)
    confirmObject.img = pygame.transform.scale(confirmObject.img, (int((confirmObject.rect.width * midBox.rect.height) 
                                    / (2 * confirmObject.rect.height)), int(midBox.rect.height / 2)))
    confirmObject.setRect()
    confirmObject.rect.move_ip(width - (confirmObject.rect.width + (midBox.rect.width / 10)), 
                                 midBox.rect.top + (midBox.rect.height / 2) - (confirmObject.rect.height / 2))
    #backspace button
    backspaceObject = imgObject(font.render('Backspace', True, black), False)
    backspaceObject.img = pygame.transform.scale(backspaceObject.img, (int((backspaceObject.rect.width * midBox.rect.height) 
                                    / (2 * backspaceObject.rect.height)), int(midBox.rect.height / 2)))
    backspaceObject.setRect()
    backspaceObject.rect.move_ip(midBox.rect.left + (midBox.rect.width / 10), 
                                 midBox.rect.top + (midBox.rect.height / 2) - (backspaceObject.rect.height / 2))
    #part that says "Name: " that we will print the character's name after
    preNameObject = imgObject(font.render('Name: ', True, black), False)
    preNameObject.img = pygame.transform.scale(preNameObject.img, (int((preNameObject.rect.width * topBox.rect.height) 
                                    / (2 * preNameObject.rect.height)), int(topBox.rect.height / 2)))
    preNameObject.setRect()
    preNameObject.rect.move_ip(topBox.rect.width / 20, (topBox.rect.height / 2) - (preNameObject.rect.height / 2))
    pygame.event.get() #get all the events, in case the player was clicking during the intro screen
    while not confirm:
        clock.tick(120) #don't let the program run wild with the framerate
        #make the name text inside the while loop so it updates as the player changes it
        nameObject = imgObject(font.render((hero.name), True, navyBlue), False)
        nameObject.img = pygame.transform.scale(nameObject.img, (int((nameObject.rect.width * topBox.rect.height) 
                                    / (2 * nameObject.rect.height)), int(topBox.rect.height / 2)))
        nameObject.setRect()
        nameObject.rect.move_ip(preNameObject.rect.width + preNameObject.rect.left, (topBox.rect.height / 2) - (nameObject.rect.height / 2))
        #update the elements on the screen
        screen.fill(white)
        screen.blit(topBox.img, topBox.rect)
        screen.blit(midBox.img, midBox.rect)
        screen.blit(botBox.img, botBox.rect)
        screen.blit(confirmObject.img, confirmObject.rect)
        screen.blit(backspaceObject.img, backspaceObject.rect)
        screen.blit(preNameObject.img, preNameObject.rect)
        screen.blit(nameObject.img, nameObject.rect)
        #indexing allows us to do this with a couple loops instead of 52 separate blit statements
        for i in range(4):
            for j in range(13):
                screen.blit(textObject[i][j].img, textObject[i][j].rect)       
        pygame.display.flip()
        #get user input
        for event in pygame.event.get():
            if event.type == pygame.QUIT: 
                gameOver()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    gameOver()
            if event.type == pygame.MOUSEBUTTONDOWN:
                pos = pygame.mouse.get_pos()
                if backspaceObject.rect.collidepoint(pos):
                    hero.name = hero.name[:-1]
                if confirmObject.rect.collidepoint(pos):
                    confirm = True
                for i in range(4):
                    for j in range(13):
                        if textObject[i][j].rect.collidepoint(pos):
                            hero.name += alphabet[i][j]
 
#runs at the start of the game, explains briefly the effects of each stat
def statExplanation():
    global screen
    proceed = False
    texts = [0,0,0,0,0]
    textObjs = [0,0,0,0,0]
    #create the box so it looks nice
    box = imgObject("images/RoundedBox.png")
    box.img = pygame.transform.scale(box.img, (int(width), int(height)))
    box.setRect()
    #print Stats: at the top
    font.set_underline(True)
    title = imgObject(font.render('Stats:', True, black), False)
    font.set_underline(False)
    title.img = pygame.transform.scale(title.img, (int((title.rect.width * box.rect.height) 
                            / (11 * title.rect.height)), int(box.rect.height / 11)))
    title.setRect()
    title.rect.move_ip(box.rect.left + (box.rect.width / 20), box.rect.top + (box.rect.height / 11))
    #these are the strings which will be rendered
    texts[0] = 'Constitution affects how many hits you can take and how well you block.'
    texts[1] = 'Strength determines how much damage you deal with physical attacks.'
    texts[2] = 'Dexterity affects your ability to hit with physical attacks and dodge all attacks (further used in the Dodge action).'
    texts[3] = 'Intelligence determines how much damage you deal with magical attacks, and affects magical effects and barriers.'
    texts[4] = 'Wisdom affects your ability to hit with magical attacks, and affects magical effects and barriers.'
    #render the strings into images and position them
    for i in range(5):
        textObjs[i] = imgObject(font.render(texts[i], True, black), False)
        textObjs[i].img = pygame.transform.scale(textObjs[i].img, (int((textObjs[i].rect.width * box.rect.height) 
                            / (33 * textObjs[i].rect.height)), int(box.rect.height / 33)))
        textObjs[i].setRect()
        textObjs[i].rect.move_ip(box.rect.left + (box.rect.width / 30), box.rect.top + (((box.rect.height / 33) * ((3*i) + 10))))
    #prepare and update the screen
    screen.fill(white)
    screen.blit(box.img, box.rect)
    screen.blit(title.img, title.rect)
    for i in range(5):
        screen.blit(textObjs[i].img, textObjs[i].rect)
    pygame.display.flip()
    #wait for user input before proceeding
    while not proceed:
        clock.tick(10)
        for event in pygame.event.get():
            if event.type == pygame.QUIT: 
                gameOver()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    gameOver()
            if event.type == pygame.MOUSEBUTTONDOWN:
                proceed = True
        
#character creation/level up screen
def levelUp(hero):
    global screen
    confirm = False
    #create the boxes used for formatting
    topBox = imgObject("images/RoundedBox.png")
    topBox.img = pygame.transform.scale(topBox.img, (int (width), int (height * 3 / 10)))
    topBox.setRect()
    
    botBox = imgObject("images/RoundedBox.png")
    botBox.img = pygame.transform.scale(botBox.img, (int (width), int (height - topBox.rect.height)))
    botBox.setRect()
    botBox.rect.move_ip(0, topBox.rect.height)
    #print the name for aesthetic
    nameObject = imgObject(font.render((hero.name), True, black), False)
    nameObject.img = pygame.transform.scale(nameObject.img, (int((nameObject.rect.width * topBox.rect.height) 
                            / (3 * nameObject.rect.height)), int(topBox.rect.height / 3)))
    nameObject.setRect()
    nameObject.rect.move_ip((topBox.rect.width / 20) + topBox.rect.left, (topBox.rect.height / 2) - (nameObject.rect.height / 2))
    #confirm button:
    confirmObject = imgObject(font.render('   Confirm', True, black), False)
    confirmObject.img = pygame.transform.scale(confirmObject.img, (int((confirmObject.rect.width * topBox.rect.height) 
                                    / (3 * confirmObject.rect.height)), int(topBox.rect.height / 3)))
    confirmObject.setRect()
    confirmObject.rect.move_ip((topBox.rect.width * 19 / 20) - confirmObject.rect.width, 
                                 topBox.rect.top + (topBox.rect.height / 2) - (confirmObject.rect.height / 2))
    #statPlusMinus will hold the name of the stats in [0][0 to 4],
    #the minus button for each stat in [1][0 to 4], the current value for the stat
    #in [2][0 to 4], and the plus button for each stat in [3][0 to 4]
    statPlusMinus = [[0 for i in range(5)],[0 for i in range(5)],[0 for i in range(5)],[0 for i in range(5)]]
    while not confirm:
        clock.tick(120) #don't let the framerate run wild
        #txts are the strings which will be rendered into images and used in imgObjects
        txts = [['Constitution','Strength','Dexterity','Intelligence','Wisdom'],['-' for i in range(5)], 
                 [str(hero.stats.con),str(hero.stats.str),str(hero.stats.dex),str(hero.stats.int),str(hero.stats.wis)], ['+' for i in range(5)]]
        #display the current available points to be spent
        pts = imgObject(font.render((str(hero.statpoints) + ' points'), True, black), False)
        pts.img = pygame.transform.scale(pts.img, (int((pts.rect.width * topBox.rect.height) 
                            / (3 * pts.rect.height)), int(topBox.rect.height / 3)))
        pts.setRect()
        pts.rect.move_ip((((topBox.rect.width * 19) / 20) + topBox.rect.left - confirmObject.rect.width) - 
                         (pts.rect.width), (topBox.rect.height / 2) - (pts.rect.height / 2))
        for i  in range(4):
            for j in range(5):
                statPlusMinus[i][j] = imgObject(font.render(txts[i][j], True, black), False)
                statPlusMinus[i][j].img = pygame.transform.scale(statPlusMinus[i][j].img, (int((statPlusMinus[i][j].rect.width * botBox.rect.height) 
                            / (11 * statPlusMinus[i][j].rect.height)), int(botBox.rect.height / 11)))
                statPlusMinus[i][j].setRect()
                if i == 0:
                    statPlusMinus[i][j].rect.move_ip((botBox.rect.width / 20) + 
                                botBox.rect.left, (botBox.rect.top + ((botBox.rect.height / 11) * (1 +(j*2)))))
                else:
                    statPlusMinus[i][j].rect.move_ip((botBox.rect.width * 19 / 20) - 
                                ((botBox.rect.height/11) * (3-i)), (botBox.rect.top + ((botBox.rect.height / 11) * (1 + (2 * j)))))
        #update the display
        screen.fill(white)
        screen.blit(topBox.img, topBox.rect)
        screen.blit(botBox.img, botBox.rect)
        screen.blit(nameObject.img, nameObject.rect)
        screen.blit(pts.img, pts.rect)
        screen.blit(confirmObject.img, confirmObject.rect)
        for i in range(4):
            for j in range(5):
                screen.blit(statPlusMinus[i][j].img, statPlusMinus[i][j].rect)
        pygame.display.flip()
        #get user input
        for event in pygame.event.get():
            if event.type == pygame.QUIT: 
                gameOver()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    gameOver()
            if event.type == pygame.MOUSEBUTTONDOWN:
                pos = pygame.mouse.get_pos()
                if confirmObject.rect.collidepoint(pos):
                    confirm = True
#unfortunately, the stats themselves aren't indexed, so these will have to be written out individually:
                if statPlusMinus[1][0].rect.collidepoint(pos) and hero.stats.con > 1:
                    hero.statpoints += 1
                    hero.stats.con -= 1
                if statPlusMinus[1][1].rect.collidepoint(pos) and hero.stats.str > 1:
                    hero.statpoints += 1
                    hero.stats.str -= 1
                if statPlusMinus[1][2].rect.collidepoint(pos) and hero.stats.dex > 1:
                    hero.statpoints += 1
                    hero.stats.dex -= 1
                if statPlusMinus[1][3].rect.collidepoint(pos) and hero.stats.int > 1:
                    hero.statpoints += 1
                    hero.stats.int -= 1
                if statPlusMinus[1][4].rect.collidepoint(pos) and hero.stats.wis > 1:
                    hero.statpoints += 1
                    hero.stats.wis -= 1
                if hero.statpoints > 0:
                    if statPlusMinus[3][0].rect.collidepoint(pos):
                        hero.statpoints -= 1
                        hero.stats.con += 1
                    if statPlusMinus[3][1].rect.collidepoint(pos):
                        hero.statpoints -= 1
                        hero.stats.str += 1
                    if statPlusMinus[3][2].rect.collidepoint(pos):
                        hero.statpoints -= 1
                        hero.stats.dex += 1
                    if statPlusMinus[3][3].rect.collidepoint(pos):
                        hero.statpoints -= 1
                        hero.stats.int += 1
                    if statPlusMinus[3][4].rect.collidepoint(pos):
                        hero.statpoints -= 1
                        hero.stats.wis += 1
    hero.stats.physHit = 10 * math.sqrt(hero.stats.dex)
    hero.stats.magHit = 10 * math.sqrt(hero.stats.wis)
                                                        
#function for creating and placing a tile
def createTile(n):
    #make the global variables locally usable
    global numTiles
    global tiles
    global openings
    global size
    #create the first tile, which will be only a 4, 3, or 2 open-sides tile
    if numTiles == 0:
        if n == 4:
            tiles[numTiles] = tile("images\Tile4.png", [1,1,1,1])
        elif n == 3:
            tiles[numTiles] = tile("images\Tile3.png", [1,1,0,1])
        elif n == 2:
            if random.randint(0,1) == 0:
                tiles[numTiles] = tile("images\Tile2.png", [1,0,0,1])
            else:
                tiles[numTiles] = tile("images\Tile20.png", [0,1,0,1])
        #move the first tile to the center of the screen
        dx = (size[0] / 2) - tiles[numTiles].rect.width / 2
        dy = (size[1] / 2) - tiles[numTiles].rect.height / 2
        tiles[numTiles].rect.move_ip(dx, dy)
    #if this isn't the first tile, it needs to be attached to an existing tile
    else:
        #use openTile to store which tile number has the opening
        openTile = 0
        #pick a random direction to expand the dungeon
        r = random.randint(0,3)
        #need a for loop to handle trying multiple directions
        #if it runs through all four directions without finding any openings,
        #then the dungeon generation is finished
        foundOpening = False
        for i in range(4):
            #need to increment r at the start, so it doesn't change after we
            #find our opening. It's random and then subjected to modulus, 
            #so it shouldn't matter (it will still be a random number from 0 to 3)
            r += 1
            r = r % 4
            for j in range(numTiles):
                if tiles[j].genSides[r] == 1:
                    #tiles can be generated with open sides towards other tiles
                    #we need to check if another tile is on the open side
                    #before placing a new one, and if a tile is there
                    #then we need to close that side on the genSides. 
                    #We also need to close the openSides if the adjacent tile
                    #is not open
                    #first, we need x and y for a point in the area that a tile would be
                    #inside another tile
                    collides = False
                    if r == 0:
                        cx = tiles[j].rect.left - 5
                        cy = tiles[j].rect.top + 5
                    elif r == 1:
                        cx = tiles[j].rect.left + 5
                        cy = tiles[j].rect.top - 5
                    elif r == 2:
                        cx = tiles[j].rect.left + tiles[j].rect.width + 5
                        cy = tiles[j].rect.top + 5
                    elif r == 3:
                        cx = tiles[j].rect.left + 5
                        cy = tiles[j].rect.top + tiles[j].rect.height + 5
                        
                    for k in range(numTiles):
                        if tiles[k].rect.collidepoint(cx, cy):
                            collides = True
                            tiles[j].genSides[r] = 0
                            #(r + 2) % 4 gives us the index of the opposite side,
                            #for the other tile
                            if tiles[k].openSides[((r + 2) % 4)] == 0:
                                tiles[j].openSides[r] = 0
                    if not collides:        
                        foundOpening = True
                        openTile = j
                        break
            if foundOpening:
                break
        #if there were no openings found, let the generate function know that
        #there are no openings, and subtract 1 from numTiles because the
        #generate function adds one after this function call
        if not foundOpening:
            openings = False
            numTiles -= 1
        #if there was an opening found, make the new tile
        else:
            if n == 1:
                tiles[numTiles] = tile("images\Tile1.png", [0,0,0,1])
            elif n == 2:
                if random.randint(0,1) == 0:
                    tiles[numTiles] = tile("images\Tile2.png", [1,0,0,1])
                else:
                    tiles[numTiles] = tile("images\Tile20.png", [0,1,0,1])
            elif n == 3:
                tiles[numTiles] = tile("images\Tile3.png", [1,1,0,1])
            elif n == 4:
                tiles[numTiles] = tile("images\Tile4.png", [1,1,1,1])
            #rotate the tile in a random*90 degree amount, then see if it fits
            #where we're trying to put it; if not, rotate again
            oldGenSides = [0,0,0,0]
            oldOpenSides = [0,0,0,0]
            ran = random.randint(0,3)
            fits = False
            while not fits:
                for i in range(4):
                    oldGenSides[i] = tiles[numTiles].genSides[i]
                    oldOpenSides[i] = tiles[numTiles].openSides[i]
                tiles[numTiles].img = pygame.transform.rotate(tiles[numTiles].img, (90 * ran))
                for i in range(4):
                    tiles[numTiles].genSides[i] = oldGenSides[((i + ran) % 4)]
                    tiles[numTiles].openSides[i] = oldOpenSides[((i + ran) % 4)]
                if tiles[numTiles].genSides[((r+2)%4)] == 1:
                    fits = True
                #if it doesn't fit, just start rotating it by 1 increment until it does
                else:
                    ran = 1
            #now that it fits, place it correctly adjacent to the opening
            dx = 0
            dy = 0
            if r == 0:
                dx = tiles[openTile].rect.left - tiles[numTiles].rect.width
                dy = tiles[openTile].rect.top
            elif r == 1:
                dx = tiles[openTile].rect.left
                dy = tiles[openTile].rect.top - tiles[numTiles].rect.height
            elif r == 2:
                dx = tiles[openTile].rect.left + tiles[openTile].rect.width
                dy = tiles[openTile].rect.top
            elif r == 3:
                dx = tiles[openTile].rect.left
                dy = tiles[openTile].rect.top + tiles[openTile].rect.height
            tiles[numTiles].rect.move_ip(dx, dy)
            tiles[openTile].genSides[r] = 0
            tiles[numTiles].genSides[((r + 2) % 4)]
            #add combat to every 20th tile, should make 20-25 max battles per dungeon
            if numTiles % 20 == 0:
                tiles[numTiles].battle = True

#create a 4 sided tile on the location of the most recently created tile
#used as part of the creation of a room area that is far away from the spawn
def forceCreateTile():
    global numTiles
    global tiles
    global openings
    numTiles -= 1
    x = tiles[numTiles].rect.left
    y = tiles[numTiles].rect.top
    tiles[numTiles] = tile("images\Tile4b.png", [1,1,1,1])
    tiles[numTiles].rect.move_ip(x,y)
    openings = True
 
#function for deleting a dungeon to make room for a new one    
def newDungeon():
    global numTiles
    global tiles
    global openings
    global firstTime
    global screen
    del tiles [:]
    newTiles()
    numTiles = 0
    firstTime = 3
    openings = True
    dungeonGenerate()
    screen.fill(black)
    for i in range(numTiles):
        screen.blit(tiles[i].img, tiles[i].rect)  
    pygame.display.flip()
    
#have to create the new tiles array in a different function or else python won't let it run
def newTiles():
    global tiles
    tiles = []    
#function for auto-creating the dungeon out of tiles
def dungeonGenerate():
    #make the global variables locally usable
    global numTiles
    global tiles
    global openings
    global firstTime
    global numDungeon
    global bossTile
    global boss
    while(openings):
        #create a slot in the tiles list for the next tile
        tiles.append(0)
        #random number decides tile generation
        g = random.randint(0,7)
        #change to tiles with less open sides as the number of tiles increases
        #so that the dungeon eventually closes off.
        #create some open space to work with at the start
        if numTiles < 30 * firstTime:
            if g < 3:
                createTile(4)
                numTiles += 1
            elif g < 5:
                createTile(3)
                numTiles += 1
            elif g < 8:
                createTile(2)
                numTiles += 1
        #start making good walls and corridors
        elif numTiles < 60 * firstTime:
            if g < 1:
                createTile(4)
                numTiles += 1
            elif g < 3:
                createTile(3)
                numTiles += 1
            elif g < 8:
                createTile(2)
                numTiles += 1
        #make less open areas, more walls/tunnels, start closing off
        elif numTiles < 100 * firstTime:
            if g < 1:
                createTile(4)
                numTiles += 1
            elif g < 3:
                createTile(3)
                numTiles += 1
            elif g < 7:
                createTile(2)
                numTiles += 1
            elif g < 8:
                createTile(1)
                numTiles += 1
        #really make more tunnels, less openings, keep closing off        
        elif numTiles < 130 * firstTime:
            if g < 3:
                createTile(4)
                numTiles += 1
            elif g < 4:
                createTile(3)
                numTiles += 1
            elif g < 7:
                createTile(2)
                numTiles += 1
            elif g < 8:
                createTile(1)
                numTiles += 1
        #at this point, just start closing off all open edges
        else:
            createTile(1)
            numTiles += 1
#this code segment is for watching the dungeon generate, for debugging
#also because it's kind of cool            
#        screen.fill(black)
#        for i in range(numTiles):
#            screen.blit(tiles[i].img, tiles[i].rect)  
#            pygame.display.flip()
#            clock.tick(30)
    #make a room/open area that should be away from the spawn
    #we will be recursing the generate function, to get a nice looking room
    #the firstTime variable prevents it from being an infinite loop of recursion
    #I also used it to modify the numTiles effect on generation, so that the
    #far room has an interesting layout
    if firstTime == 3:
        firstTime = 4
        #createTile won't work because there are no available open sides
        #so we force a 4 sided tile to spawn on the last tile placed
        forceCreateTile()
        numTiles += 1
        #now that we have open sides, we create 20 more 4 sided tiles to really
        #open up a room, tile 15 will be special (boss fight)
        for i in range(20):
            tiles.append(0)
            if i == 15:
                createTile(4)
                bossTile = numTiles
            else:
                createTile(4)
            numTiles += 1
        #then we recurse the function to finish the room with a few more tiles
        dungeonGenerate()
    else:
        numDungeon += 1
    boss = imgObject("images/bossIcon.png")
    boss.img = pygame.transform.scale(boss.img, (int((tiles[bossTile].rect.width / 3)), int(tiles[bossTile].rect.height / 3)))
    boss.setRect()
    boss.rect = pygame.Rect(tiles[bossTile].rect.left, tiles[bossTile].rect.top, boss.rect.width, boss.rect.height)
    
#function for handling attacks, returns 0 on a miss, t arg is the type of attack
def attack(attacker, target, t = 0):
    if t > 0:
        attackHitMod = attacker.stats.magHit
        dmgStat = attacker.stats.int
    else:
        attackHitMod = attacker.stats.physHit
        dmgStat = attacker.stats.str
        
    r = random.randint(1,100)
    if (((r + attackHitMod) > (40 + target.stats.dodge)) 
        or r > 95):
        #the target takes damage based on attacker strength
        #more if the target is burned; less if the target is
        #blocking or barrier-ing, and less if the hero
        #is frozen
        base = (random.randint(1,dmgStat) +
                random.randint(1,dmgStat) + attacker.stats.lvl)
        #do multiplicative modifiers first, flat second
        total = (base * (1 + (target.stats.burn/100)) * (1 - (attacker.stats.freeze/100))
                  * (1 - (target.stats.barrier/100)))
        
        if target.name == 'Goblin' and t == 0:
            total = total * 1.5
        elif target.name == 'Wraith' and t == 0:
            total = total / 2
        elif target.name == 'Troll' and t == 1:
            total = total * 1.5
        elif target.name == 'Fiend' and t == 2:
            total = total * 1.5
        elif target.name == 'Ogre' and t == 3:
            total = total * 1.5
            
        if attacker.name == 'Ogre' and t == 0:
            total += attacker.stats.str / 2
            
        total -= target.stats.blocked
        
        if total < 1:
            total = 1
        else:
            total = int(round(total))
            target.stats.HP -= total
    else:
        total = 0
    return total

#function for combat, n is the base stats of the enemy if you want to generate one, 
#foe is so you can pass an enemy that is already generated
def battle(n = 1, foe = 0):
    if foe == 0:
        foe = enemy(n)
    fighting = True
    textScale = 6
     #create the enemy for the battle
    print(foe.name, foe.stats.con, foe.stats.str, foe.stats.dex, foe.stats.int, foe.stats.wis)
    #we will create everything we need for battle at the start, and then
    #blit them in as necessary
    
    #set location and size for the enemy sprite
    foe.img = pygame.transform.scale(foe.img, (int(width / 3), int(((foe.rect.height * width) / (foe.rect.width * 3)))))
    foe.rect = foe.img.get_rect()
    foe.rect.move_ip(width/3, height / 9)
    #box for the enemy name and health bar
    topLeftBox = imgObject("images/RoundedBox.png")
    topLeftBox.img = pygame.transform.scale(topLeftBox.img, (int(width/3), int(height/6)))
    topLeftBox.setRect()
    #box for combat-related text
    botLeftBox = imgObject("images/RoundedBox.png")
    botLeftBox.img = pygame.transform.scale(botLeftBox.img, (int (width*2/3), int (height/6)))
    botLeftBox.setRect()
    botLeftBox.rect.move_ip(0, height - botLeftBox.rect.height)
    #box for the player commands
    botRightBox = imgObject("images/RoundedBox.png")
    botRightBox.img = pygame.transform.scale(botRightBox.img, (int(width/3), int(height/3)))
    botRightBox.setRect()
    botRightBox.rect.move_ip(width - botRightBox.rect.width, height - botRightBox.rect.height)
    #box for the player name and health bar
    midRightBox = imgObject("images/RoundedBox.png")
    midRightBox.img = pygame.transform.scale(midRightBox.img, (int(width/3), int(height/6)))
    midRightBox.setRect()
    midRightBox.rect.move_ip(width - midRightBox.rect.width, height - (
        botRightBox.rect.height + midRightBox.rect.height))
    #health bars
    enemyHealthBar = imgObject("images/blackBar.png")
    enemyHealthBar.img = pygame.transform.scale(enemyHealthBar.img, 
        (int((topLeftBox.rect.width * 4) / 5), int(topLeftBox.rect.height / 3)))
    enemyHealthBar.setRect()
    enemyHealthBar.rect.move_ip((topLeftBox.rect.width / 10), (topLeftBox.rect.height / 2))
    
    heroHealthBar = imgObject("images/blackBar.png")
    heroHealthBar.img = pygame.transform.scale(heroHealthBar.img, 
        (int((midRightBox.rect.width * 4) / 5), int(midRightBox.rect.height / 3)))
    heroHealthBar.setRect()
    heroHealthBar.rect.move_ip((midRightBox.rect.left + (midRightBox.rect.width / 10)), 
                               (midRightBox.rect.top + (midRightBox.rect.height / 2)))
    
    #ahead lies a whole bunch of text rendering
    heroNameText = imgObject(font.render(hero.name, True, black), False)
    heroNameText.img = pygame.transform.scale(heroNameText.img, (int((midRightBox.rect.height 
                    * heroNameText.rect.width)/(3 * heroNameText.rect.height)), int(midRightBox.rect.height/3)))
    heroNameText.setRect()
    heroNameText.rect.move_ip((midRightBox.rect.left + (midRightBox.rect.width / 10)), 
                              (midRightBox.rect.top + (midRightBox.rect.height / 9)))
    
    foeNameText = imgObject(font.render(foe.name, True, black), False)
    foeNameText.img = pygame.transform.scale(foeNameText.img, (int((topLeftBox.rect.height 
                    * foeNameText.rect.width)/(3 * foeNameText.rect.height)), int(topLeftBox.rect.height/3)))
    foeNameText.setRect()
    foeNameText.rect.move_ip((topLeftBox.rect.left + (topLeftBox.rect.width / 10)), 
                              (topLeftBox.rect.top + (topLeftBox.rect.height / 9)))
    
    backText = imgObject(font.render('Back', True, black), False)
    backText.img = pygame.transform.scale(backText.img, (int((botRightBox.rect.height 
                    * backText.rect.width)/(textScale * backText.rect.height)), int(botRightBox.rect.height/textScale)))
    backText.setRect()
    backText.rect.move_ip(width - (botRightBox.rect.width * 4 / 9), height - (
        (botRightBox.rect.height * 4 / 9)))
    
    itemText = imgObject(font.render('Item', True, black), False)
    itemText.img = pygame.transform.scale(itemText.img, (int((botRightBox.rect.height 
                    * itemText.rect.width)/(textScale * itemText.rect.height)), int(botRightBox.rect.height/textScale)))
    itemText.setRect()
    itemText.rect.move_ip(width - (botRightBox.rect.width * 4 / 9), height - (
        (botRightBox.rect.height * 4 / 9)))
    
    atkText = imgObject(font.render('Attack', True, black), False)
    atkText.img = pygame.transform.scale(atkText.img, (int((botRightBox.rect.height 
                    * atkText.rect.width)/(textScale * atkText.rect.height)), int(botRightBox.rect.height/textScale)))
    atkText.setRect()
    atkText.rect.move_ip(width - (botRightBox.rect.width * 8 / 9), height - (
        (botRightBox.rect.height * 7 / 9)))

    confirmText = imgObject(font.render('Confirm', True, black), False)
    confirmText.img = pygame.transform.scale(confirmText.img, (int((botRightBox.rect.height 
                    * confirmText.rect.width)/(textScale * confirmText.rect.height)), int(botRightBox.rect.height/textScale)))
    confirmText.setRect()
    confirmText.rect.move_ip(width - (botRightBox.rect.width * 8 / 9), height - (
        (botRightBox.rect.height * 7 / 9)))
    
    barrierText = imgObject(font.render('Barrier', True, black), False)
    barrierText.img = pygame.transform.scale(barrierText.img, (int((botRightBox.rect.height 
                    * barrierText.rect.width)/(textScale * barrierText.rect.height)), int(botRightBox.rect.height/textScale)))
    barrierText.setRect()
    barrierText.rect.move_ip(width - (botRightBox.rect.width * 8 / 9), height - (
        (botRightBox.rect.height * 7 / 9)))
    
    fireText = imgObject(font.render('Fire', True, black), False)
    fireText.img = pygame.transform.scale(fireText.img, (int((botRightBox.rect.height 
                    * fireText.rect.width)/(textScale * fireText.rect.height)), int(botRightBox.rect.height/textScale)))
    fireText.setRect()
    fireText.rect.move_ip(width - (botRightBox.rect.width * 8 / 9), height - (
        (botRightBox.rect.height * 7 / 9)))
    
    noneText = imgObject(font.render('None', True, black), False)
    noneText.img = pygame.transform.scale(noneText.img, (int((botRightBox.rect.height 
                    * noneText.rect.width)/(textScale * noneText.rect.height)), int(botRightBox.rect.height/textScale)))
    noneText.setRect()
    noneText.rect.move_ip(width - (botRightBox.rect.width * 8 / 9), height - (
        (botRightBox.rect.height * 7 / 9)))

    defText = imgObject(font.render('Defend', True, black), False)
    defText.img = pygame.transform.scale(defText.img, (int((botRightBox.rect.height 
                    * defText.rect.width)/(textScale * defText.rect.height)), int(botRightBox.rect.height/textScale)))
    defText.setRect()
    defText.rect.move_ip(width - (botRightBox.rect.width * 4 / 9), height - (
        (botRightBox.rect.height * 7 / 9)))
    
    dodgeText = imgObject(font.render('Dodge', True, black), False)
    dodgeText.img = pygame.transform.scale(dodgeText.img, (int((botRightBox.rect.height 
                    * dodgeText.rect.width)/(textScale * dodgeText.rect.height)), int(botRightBox.rect.height/textScale)))
    dodgeText.setRect()
    dodgeText.rect.move_ip(width - (botRightBox.rect.width * 4 / 9), height - (
        (botRightBox.rect.height * 7 / 9)))
    
    lightningText = imgObject(font.render('Lightning', True, black), False)
    lightningText.img = pygame.transform.scale(lightningText.img, (int((botRightBox.rect.height 
                    * lightningText.rect.width)/(textScale * lightningText.rect.height)), int(botRightBox.rect.height/textScale)))
    lightningText.setRect()
    lightningText.rect.move_ip(width - (botRightBox.rect.width * 4 / 9), height - (
        (botRightBox.rect.height * 7 / 9)))
    
    magicText = imgObject(font.render('Magic', True, black), False)
    magicText.img = pygame.transform.scale(magicText.img, (int((botRightBox.rect.height 
                    * magicText.rect.width)/(textScale * magicText.rect.height)), int(botRightBox.rect.height/textScale)))
    magicText.setRect()
    magicText.rect.move_ip(width - (botRightBox.rect.width * 8 / 9), height - (
        (botRightBox.rect.height * 4 / 9)))
    
    blockText = imgObject(font.render('Block', True, black), False)
    blockText.img = pygame.transform.scale(blockText.img, (int((botRightBox.rect.height 
                    * blockText.rect.width)/(textScale * blockText.rect.height)), int(botRightBox.rect.height/textScale)))
    blockText.setRect()
    blockText.rect.move_ip(width - (botRightBox.rect.width * 8 / 9), height - (
        (botRightBox.rect.height * 4 / 9)))
    
    iceText = imgObject(font.render('Ice', True, black), False)
    iceText.img = pygame.transform.scale(iceText.img, (int((botRightBox.rect.height 
                    * iceText.rect.width)/(textScale * iceText.rect.height)), int(botRightBox.rect.height/textScale)))
    iceText.setRect()
    iceText.rect.move_ip(width - (botRightBox.rect.width * 8 / 9), height - (
        (botRightBox.rect.height * 4 / 9)))
    
    botLeftString = (foe.name + ' appeared!')
    botLeftText = imgObject(font.render(botLeftString, True, black), False)
    botLeftText.img = pygame.transform.scale(botLeftText.img, (int((botLeftBox.rect.height 
                    * botLeftText.rect.width)/(textScale * botLeftText.rect.height)), int(botLeftBox.rect.height/textScale)))
    botLeftText.setRect()
    botLeftText.rect.move_ip((botLeftBox.rect.left + (botLeftBox.rect.width / 12)), 
                             botLeftBox.rect.top + ((botLeftBox.rect.height / 2) - (botLeftText.rect.height / 2)))
    #battleText holds which text box the battle is in
    #0 is for the starting box, 1 is attack, 2 is Defend, 3 is Magic, 4 is Item
    battleText = 0
    #turn holds whose turn it is, 0 for the hero, 1 for the enemy
    turn = random.randint(0,1)
    actions = 1 #holds how many actions the hero/enemy has left
    spellcaster = foe.name == 'Fiend' #only fiend enemies cast spells
    while fighting:
        enemyHealthBar.img = pygame.transform.scale(enemyHealthBar.img, 
                (int(((topLeftBox.rect.width * 4) / 5) * (foe.stats.HP / foe.stats.maxHP)), int(topLeftBox.rect.height / 3)))
        enemyHealthBar.setRect()
        enemyHealthBar.rect.move_ip((topLeftBox.rect.width / 10), (topLeftBox.rect.height / 2))
        heroHealthBar.img = pygame.transform.scale(heroHealthBar.img, 
                (int(((midRightBox.rect.width * 4) / 5) * (hero.stats.HP / hero.stats.maxHP)), int(midRightBox.rect.height / 3)))
        heroHealthBar.setRect()
        heroHealthBar.rect.move_ip((midRightBox.rect.left + (midRightBox.rect.width / 10)), 
                           (midRightBox.rect.top + (midRightBox.rect.height / 2)))
        screen.fill(white)
        screen.blit(foe.img, foe.rect)
        screen.blit(topLeftBox.img, topLeftBox.rect)
        screen.blit(botLeftBox.img, botLeftBox.rect)
        screen.blit(botRightBox.img, botRightBox.rect)
        screen.blit(midRightBox.img, midRightBox.rect)
        screen.blit(botLeftText.img, botLeftText.rect)
        screen.blit(enemyHealthBar.img, enemyHealthBar.rect)
        screen.blit(heroHealthBar.img, heroHealthBar.rect)
        screen.blit(heroNameText.img, heroNameText.rect)
        screen.blit(foeNameText.img, foeNameText.rect)
        pygame.display.flip()
        #only keep defense bonuses for 1 turn
        hero.stats.resetDefenses()
        turnOffFoeBurn = foe.stats.burn > 0
        pygame.event.get()
        while turn == 0 and hero.stats.HP > 0 and foe.stats.HP > 0:
            clock.tick(30)
            if hero.stats.stun:
                actions -= 1
                hero.stats.stun = False
            enemyHealthBar.img = pygame.transform.scale(enemyHealthBar.img, 
                (int(((topLeftBox.rect.width * 4) / 5) * (foe.stats.HP / foe.stats.maxHP)), int(topLeftBox.rect.height / 3)))
            enemyHealthBar.setRect()
            enemyHealthBar.rect.move_ip((topLeftBox.rect.width / 10), (topLeftBox.rect.height / 2))
            heroHealthBar.img = pygame.transform.scale(heroHealthBar.img, 
                (int(((midRightBox.rect.width * 4) / 5) * (hero.stats.HP / hero.stats.maxHP)), int(midRightBox.rect.height / 3)))
            heroHealthBar.setRect()
            heroHealthBar.rect.move_ip((midRightBox.rect.left + (midRightBox.rect.width / 10)), 
                               (midRightBox.rect.top + (midRightBox.rect.height / 2)))
            screen.fill(white)
            screen.blit(foe.img, foe.rect)
            screen.blit(topLeftBox.img, topLeftBox.rect)
            screen.blit(botLeftBox.img, botLeftBox.rect)
            screen.blit(botRightBox.img, botRightBox.rect)
            screen.blit(midRightBox.img, midRightBox.rect)
            screen.blit(botLeftText.img, botLeftText.rect)
            screen.blit(enemyHealthBar.img, enemyHealthBar.rect)
            screen.blit(heroHealthBar.img, heroHealthBar.rect)
            screen.blit(heroNameText.img, heroNameText.rect)
            screen.blit(foeNameText.img, foeNameText.rect)
            
            if battleText == 0:
                screen.blit(atkText.img, atkText.rect)
                screen.blit(defText.img, defText.rect)
                screen.blit(magicText.img, magicText.rect)
                screen.blit(itemText.img, itemText.rect)
            elif battleText == 1:
                screen.blit(confirmText.img, confirmText.rect)
                screen.blit(backText.img, backText.rect)
            elif battleText == 2:
                screen.blit(barrierText.img, barrierText.rect)
                screen.blit(dodgeText.img, dodgeText.rect)
                screen.blit(blockText.img, blockText.rect)
                screen.blit(backText.img, backText.rect)
            elif battleText == 3:
                screen.blit(fireText.img, fireText.rect)
                screen.blit(lightningText.img, lightningText.rect)
                screen.blit(iceText.img, iceText.rect)
                screen.blit(backText.img, backText.rect)
            elif battleText == 4:
                screen.blit(backText.img, backText.rect)
            #still need to be able to exit the game
            for event in pygame.event.get():
                if event.type == pygame.QUIT: 
                    gameOver()
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        fighting = False
                if event.type == pygame.MOUSEBUTTONDOWN:
                    pos = pygame.mouse.get_pos()
                    
                    if battleText == 0:
                        if atkText.rect.collidepoint(pos):
                            battleText = 1
                        elif defText.rect.collidepoint(pos):
                            battleText = 2
                        elif magicText.rect.collidepoint(pos):
                            battleText = 3
                        elif itemText.rect.collidepoint(pos):
                            battleText = 4
                            
                    elif battleText == 1:
                        if confirmText.rect.collidepoint(pos):
                            total = attack(hero, foe)
                            if total > 0:
                                botLeftString = (foe.name + ' takes ' + str(total) + ' physical damage')
                            else:
                                botLeftString = (hero.name + "'s attack misses")
                            actions -= 1
                            battleText = 0
                        if backText.rect.collidepoint(pos):
                            battleText = 0
                            
                    elif battleText == 2:
                        if barrierText.rect.collidepoint(pos):
                            botLeftString = (hero.name + " summons a protective barrier")
                            hero.stats.barrier = 5 * math.sqrt(hero.stats.int * hero.stats.wis)
                            actions -= 1
                            battleText = 0
                        if dodgeText.rect.collidepoint(pos):
                            botLeftString = (hero.name + " prepares to dodge")
                            hero.stats.dodge += hero.stats.dex
                            actions -= 1
                            battleText = 0
                        if blockText.rect.collidepoint(pos):
                            botLeftString = (hero.name + " prepares to block")
                            hero.stats.blocked += hero.stats.con
                            actions -= 1
                            battleText = 0
                        if backText.rect.collidepoint(pos):
                            battleText = 0
                            
                    elif battleText == 3:
                        if fireText.rect.collidepoint(pos):
                            total = attack(hero, foe, 1)
                            if total > 0:
                                botLeftString = (foe.name + ' takes ' + str(total) + ' fire damage')
                                if random.randint(1,100) < (hero.stats.int + hero.stats.wis):
                                    foe.stats.burn = 2 * math.sqrt(hero.stats.int + hero.stats.wis)
                                    turnOffFoeBurn = False
                                    botLeftString += ' and is burned'
                            else:
                                botLeftString = (hero.name + "'s fire magic misses")
                            actions -= 1
                            battleText = 0
                            
                        if lightningText.rect.collidepoint(pos):
                            total = attack(hero, foe, 2)
                            if total > 0:
                                botLeftString = (foe.name + ' takes ' + str(total) + ' lightning damage')
                                #added spell effect
                                if random.randint(1,100) < (hero.stats.int + hero.stats.wis):
                                    foe.stats.stun = True
                                    botLeftString += ' and is stunned'
                            else:
                                botLeftString = (hero.name + "'s lightning magic misses")
                            actions -= 1
                            battleText = 0
                            
                        if iceText.rect.collidepoint(pos):
                            total = attack(hero, foe, 3)
                            if total > 0:
                                botLeftString = (foe.name + ' takes ' + str(total) + ' ice damage')
                                #added spell effect
                                if random.randint(1,100) < (hero.stats.int + hero.stats.wis):
                                    foe.stats.freeze = 2 * math.sqrt(hero.stats.int + hero.stats.wis)
                                    botLeftString += ' and is chilled'
                            else:
                                botLeftString = (hero.name + "'s ice magic misses")
                            actions -= 1
                            battleText = 0
                        if backText.rect.collidepoint(pos):
                            battleText = 0
                            
                    elif battleText == 4:
                        if backText.rect.collidepoint(pos):
                            battleText = 0
            
            botLeftText = imgObject(font.render(botLeftString, True, black), False)
            botLeftText.img = pygame.transform.scale(botLeftText.img, (int((botLeftBox.rect.height 
                    * botLeftText.rect.width)/(textScale * botLeftText.rect.height)), int(botLeftBox.rect.height/textScale)))
            botLeftText.setRect()
            botLeftText.rect.move_ip((botLeftBox.rect.left + (botLeftBox.rect.width / 12)), 
                    botLeftBox.rect.top + ((botLeftBox.rect.height / 2) - (botLeftText.rect.height / 2)))
            if actions == 0:
                turn = 1
                actions = 2
            pygame.display.flip()
            hero.stats.freeze = 0
            if turnOffFoeBurn:
                foe.stats.burn = 0
            #end of hero turn loop
        
        if foe.stats.HP < 0 :
            foe.stats.HP = 0
        if hero.stats.HP < 0:
            hero.stats.HP = 0
        enemyHealthBar.img = pygame.transform.scale(enemyHealthBar.img, 
            (int(((topLeftBox.rect.width * 4) / 5) * (foe.stats.HP / foe.stats.maxHP)), int(topLeftBox.rect.height / 3)))
        enemyHealthBar.setRect()
        enemyHealthBar.rect.move_ip((topLeftBox.rect.width / 10), (topLeftBox.rect.height / 2))
        heroHealthBar.img = pygame.transform.scale(heroHealthBar.img, 
            (int(((midRightBox.rect.width * 4) / 5) * (hero.stats.HP / hero.stats.maxHP)), int(midRightBox.rect.height / 3)))
        heroHealthBar.setRect()
        heroHealthBar.rect.move_ip((midRightBox.rect.left + (midRightBox.rect.width / 10)), 
                       (midRightBox.rect.top + (midRightBox.rect.height / 2)))
        screen.fill(white)
        screen.blit(foe.img, foe.rect)
        screen.blit(topLeftBox.img, topLeftBox.rect)
        screen.blit(botLeftBox.img, botLeftBox.rect)
        screen.blit(botRightBox.img, botRightBox.rect)
        screen.blit(midRightBox.img, midRightBox.rect)
        screen.blit(botLeftText.img, botLeftText.rect)
        screen.blit(enemyHealthBar.img, enemyHealthBar.rect)
        screen.blit(heroHealthBar.img, heroHealthBar.rect)
        screen.blit(heroNameText.img, heroNameText.rect)
        screen.blit(foeNameText.img, foeNameText.rect)
        pygame.display.flip() 
        
        foe.stats.resetDefenses()   #reset foe defense bonuses
        if foe.name == 'Goblin':    #goblins get permanent dodge boosts
            foe.stats.dodge += foe.stats.dex
        #alreadyDid is used to prevent foe from doing the same defense twice
        #0 is block, 1 is dodge, 2 is barrier
        alreadyDid = [False, False, False] 
        #handling the turning off of burn is tricky
        turnOffHeroBurn = hero.stats.burn > 0
        while turn == 1 and foe.stats.HP > 0 and hero.stats.HP > 0:
            clock.tick(1)
            actions -= 1
            a = random.randint(1,6)
            if a == 1 and not alreadyDid[0]: #block
                botLeftString = (foe.name + " prepares to block")
                foe.stats.blocked += foe.stats.con
                alreadyDid[0] = True
            elif a == 2 and not alreadyDid[1]:  #dodge
                botLeftString = (foe.name + " prepares to dodge")
                foe.stats.dodge += foe.stats.dex
                alreadyDid[1] = True
            elif a == 3 and not alreadyDid[2] and spellcaster: #barrier
                botLeftString = (foe.name + " summons a protective barrier")
                foe.stats.barrier = 5 * math.sqrt(foe.stats.int * foe.stats.wis)
            elif spellcaster: #cast spell
                if a == 4:  #ice
                    total = attack(foe, hero, 3)
                    if total > 0:
                        botLeftString = (hero.name + ' takes ' + str(total) + ' ice damage')
                        #added spell effect
                        if random.randint(1,100) < (foe.stats.int + foe.stats.wis):
                            hero.stats.freeze = 2 * math.sqrt(foe.stats.int + foe.stats.wis)
                            botLeftString += ' and is chilled'
                    else:
                        botLeftString = (foe.name + "'s ice magic misses")
                        
                elif a == 5:    #lightning
                    total = attack(foe, hero, 2)
                    if total > 0:
                        botLeftString = (hero.name + ' takes ' + str(total) + ' lightning damage')
                        #added spell effect
                        if random.randint(1,100) < (foe.stats.int + foe.stats.wis):
                            hero.stats.stun = True
                            botLeftString += ' and is stunned'
                    else:
                        botLeftString = (foe.name + "'s lightning magic misses")
                
                else:   #fire
                    total = attack(foe, hero, 1)
                    if total > 0:
                        botLeftString = (hero.name + ' takes ' + str(total) + ' fire damage')
                        #added spell effect
                        if random.randint(1,100) < (foe.stats.int + foe.stats.wis):
                            hero.stats.burn = 2 * math.sqrt(foe.stats.int + foe.stats.wis)
                            turnOffHeroBurn = False
                            botLeftString += ' and is burned'
                    else:
                        botLeftString = (foe.name + "'s fire magic misses")
                        
            else: #physical attack
                total = attack(foe, hero)
                if total > 0:
                    botLeftString = (hero.name + ' takes ' + str(total) + ' physical damage')
                else:
                    botLeftString = (foe.name + "'s attack misses")
                    
            if foe.stats.HP < 0 :
                foe.stats.HP = 0
            if hero.stats.HP < 0:
                hero.stats.HP = 0
            enemyHealthBar.img = pygame.transform.scale(enemyHealthBar.img, 
                        (int(((topLeftBox.rect.width * 4) / 5) * (foe.stats.HP / foe.stats.maxHP)), int(topLeftBox.rect.height / 3)))
            enemyHealthBar.setRect()
            enemyHealthBar.rect.move_ip((topLeftBox.rect.width / 10), (topLeftBox.rect.height / 2))
            heroHealthBar.img = pygame.transform.scale(heroHealthBar.img, 
                        (int(((midRightBox.rect.width * 4) / 5) * (hero.stats.HP / hero.stats.maxHP)), int(midRightBox.rect.height / 3)))
            heroHealthBar.setRect()
            heroHealthBar.rect.move_ip((midRightBox.rect.left + (midRightBox.rect.width / 10)), 
                        (midRightBox.rect.top + (midRightBox.rect.height / 2)))
            
            botLeftText = imgObject(font.render(botLeftString, True, black), False)
            botLeftText.img = pygame.transform.scale(botLeftText.img, (int((botLeftBox.rect.height 
                    * botLeftText.rect.width)/(textScale * botLeftText.rect.height)), int(botLeftBox.rect.height/textScale)))
            botLeftText.setRect()
            botLeftText.rect.move_ip((botLeftBox.rect.left + (botLeftBox.rect.width / 12)), 
                    botLeftBox.rect.top + ((botLeftBox.rect.height / 2) - (botLeftText.rect.height / 2)))
            
            screen.fill(white)
            screen.blit(foe.img, foe.rect)
            screen.blit(topLeftBox.img, topLeftBox.rect)
            screen.blit(botLeftBox.img, botLeftBox.rect)
            screen.blit(botLeftText.img, botLeftText.rect)
            screen.blit(botRightBox.img, botRightBox.rect)
            screen.blit(midRightBox.img, midRightBox.rect)
            screen.blit(enemyHealthBar.img, enemyHealthBar.rect)
            screen.blit(heroHealthBar.img, heroHealthBar.rect)
            screen.blit(heroNameText.img, heroNameText.rect)
            screen.blit(foeNameText.img, foeNameText.rect)
            if actions == 0:
                if foe.name == 'Troll': #trolls regen every turn
                    foe.stats.HP += hero.stats.lvl
                turn = 0
                actions = 2
                clock.tick(1)
            pygame.display.flip()
            foe.stats.freeze = 0
            #if the hero wasn't burned again this turn, get rid of their burn
            if turnOffHeroBurn:
                hero.stats.burn = 0
            #end of enemy turn loop
        if not foe.stats.HP > 0:
            fighting = False
            clock.tick(1)
            botLeftText.img.fill(white)
            screen.blit(botLeftText.img, botLeftText.rect)
            botLeftText = imgObject(font.render((foe.name + ' dies'), True, black), False)
            botLeftText.img = pygame.transform.scale(botLeftText.img, (int((botLeftBox.rect.height 
                    * botLeftText.rect.width)/(textScale * botLeftText.rect.height)), int(botLeftBox.rect.height/textScale)))
            botLeftText.setRect()
            botLeftText.rect.move_ip((botLeftBox.rect.left + (botLeftBox.rect.width / 12)), 
                    botLeftBox.rect.top + ((botLeftBox.rect.height / 2) - (botLeftText.rect.height / 2)))
            screen.blit(botLeftText.img, botLeftText.rect)
            pygame.display.flip()
            exp = (foe.stats.con + foe.stats.str + foe.stats.dex + foe.stats.int + foe.stats.wis)
            hero.xp += exp
            
            clock.tick(1)
            botLeftText.img.fill(white)
            screen.blit(botLeftText.img, botLeftText.rect)
            botLeftText = imgObject(font.render(hero.name + ' gains ' + str(exp) + ' experience', True, black), False)
            botLeftText.img = pygame.transform.scale(botLeftText.img, (int((botLeftBox.rect.height 
                    * botLeftText.rect.width)/(textScale * botLeftText.rect.height)), int(botLeftBox.rect.height/textScale)))
            botLeftText.setRect()
            botLeftText.rect.move_ip((botLeftBox.rect.left + (botLeftBox.rect.width / 12)), 
                    botLeftBox.rect.top + ((botLeftBox.rect.height / 2) - (botLeftText.rect.height / 2)))
            screen.blit(botLeftText.img, botLeftText.rect)
            pygame.display.flip()
            clock.tick(1)
            
        if not hero.stats.HP > 0:
            fighting = False
            clock.tick(1)
            botLeftText.img.fill(white)
            screen.blit(botLeftText.img, botLeftText.rect)
            botLeftText = imgObject(font.render((hero.name + ' dies'), True, black), False)
            botLeftText.img = pygame.transform.scale(botLeftText.img, (int((botLeftBox.rect.height 
                    * botLeftText.rect.width)/(textScale * botLeftText.rect.height)), int(botLeftBox.rect.height/textScale)))
            botLeftText.setRect()
            botLeftText.rect.move_ip((botLeftBox.rect.left + (botLeftBox.rect.width / 12)), 
                    botLeftBox.rect.top + ((botLeftBox.rect.height / 2) - (botLeftText.rect.height / 2)))
            screen.blit(botLeftText.img, botLeftText.rect)
            pygame.display.flip()
            exp = (foe.stats.con + foe.stats.str + foe.stats.dex + foe.stats.int + foe.stats.wis)
            hero.xp += exp
            
            clock.tick(1)
            botLeftText.img.fill(white)
            screen.blit(botLeftText.img, botLeftText.rect)
            botLeftText = imgObject(font.render('The dungeon has taken another life.', True, black), False)
            botLeftText.img = pygame.transform.scale(botLeftText.img, (int((botLeftBox.rect.height 
                    * botLeftText.rect.width)/(textScale * botLeftText.rect.height)), int(botLeftBox.rect.height/textScale)))
            botLeftText.setRect()
            botLeftText.rect.move_ip((botLeftBox.rect.left + (botLeftBox.rect.width / 12)), 
                    botLeftBox.rect.top + ((botLeftBox.rect.height / 2) - (botLeftText.rect.height / 2)))
            screen.blit(botLeftText.img, botLeftText.rect)
            pygame.display.flip()
            clock.tick(1)
            clock.tick(1)
            gameOver()
           
#pygame initiation stuff
pygame.init()
clock = pygame.time.Clock()
displayInfo = pygame.display.Info()
size = width, height = displayInfo.current_w, displayInfo.current_h
flags = pygame.FULLSCREEN | pygame.DOUBLEBUF
#flags = pygame.DOUBLEBUF
#size = width, height = 640, 480
screen = pygame.display.set_mode(size, flags)
screen.set_alpha(None)
#run the intro in a separate thread so we can load the dungeon during the intro
intro = threading.Thread(target=introScreen, args=()) 
intro.start()
font = pygame.font.SysFont(None, 144)
pygame.event.set_allowed(pygame.QUIT)
pygame.event.set_allowed(pygame.KEYDOWN)
pygame.event.set_allowed(pygame.MOUSEBUTTONDOWN)
bossTile = 0 #holds what tile the boss generates on
#generate the dungeon
dungeonGenerate()
#currentTiles is the set of tiles that the main character is on 
#(from 1 possibly up to 4), with the first element telling us how many there are
currentTiles = [1,0,0,0,0]
#screenTiles will hold the indexes of the tiles that are on screen, so we don't
#waste resources blitting offscreen tiles
screenTiles = [0 for i in range(numTiles + 1)]
print(numTiles)
#create the hero in the center of the screen
hero = player(width/2, height/2)
intro.join()
nameCharacter()
statExplanation()
levelUp(hero)
#blit all the tiles in to start
screen.fill(black)
for i in range(numTiles):
    screen.blit(tiles[i].img, tiles[i].rect)  
pygame.display.flip()
while 1:
    #tick sets the framerate, imgSwitch[2] alternates the character frames
    clock.tick(30)
    hero.imgSwitch[2] += 1
    #this is the number of pixels the character moves per frame
    moveSpeed = 20
    #figure out which tiles the character is on
    currentTiles[0] = 0
    for i in range(numTiles):
        if hero.rect.colliderect(tiles[i]):
            currentTiles[0] += 1
            currentTiles[currentTiles[0]] = i  
    #find the furthest tile/s in each direction, and if that direction is open
    #dirBlocked holds the furthest the player can travel in a direction, or
    #zero/1000 if that direction is basically free
    dirBlocked = [0,0, width,height]
    for i in range(currentTiles[0]):
        #if the direction for the tile is blocked, set the block
        #you shouldn't be able to walk onto a tile where you would need to compare to
        #a previous block
        if tiles[currentTiles[i+1]].openSides[0] == 0:
            dirBlocked[0] = tiles[currentTiles[i+1]].rect.left
        if tiles[currentTiles[i+1]].openSides[1] == 0:
            dirBlocked[1] = tiles[currentTiles[i+1]].rect.top
        if tiles[currentTiles[i+1]].openSides[2] == 0:
            dirBlocked[2] = tiles[currentTiles[i+1]].rect.left + tiles[currentTiles[i+1]].rect.width
        if tiles[currentTiles[i+1]].openSides[3] == 0:
            dirBlocked[3] = tiles[currentTiles[i+1]].rect.top + tiles[currentTiles[i+1]].rect.height 

    #the only keypress events we're interested in are escape key presses
    #trying to use keypress events for movement was really unreliable                        
    for event in pygame.event.get():
        if event.type == pygame.QUIT: 
            gameOver()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                gameOver()
    #needBlit is used so we only blit the whole screen when we actually move the screen
    needBlit = False       
    
    keys = pygame.key.get_pressed()
    if keys[pygame.K_UP]:
        #set facing to up
        hero.imgSwitch[0] = 1
        for i in range(numTiles):
            tiles[i].rect.move_ip(0, moveSpeed)
        dirBlocked[1] += moveSpeed
        needBlit = True
        
    if keys[pygame.K_DOWN]:
        #set facing to down
        hero.imgSwitch[0] = 3
        for i in range(numTiles):
            tiles[i].rect.move_ip(0, -moveSpeed)
        dirBlocked[3] -= moveSpeed
        needBlit = True
            
    if keys[pygame.K_LEFT]:
        #set the facing to left
        hero.imgSwitch[0] = 0
        for i in range(numTiles):
            tiles[i].rect.move_ip(moveSpeed, 0)
        dirBlocked[0] += moveSpeed
        needBlit = True
            
    if keys[pygame.K_RIGHT]:
        #set the facing to the right
        hero.imgSwitch[0] = 2
        for i in range(numTiles):
            tiles[i].rect.move_ip(-moveSpeed, 0)
        dirBlocked[2] -= moveSpeed
        needBlit = True
    
        #don't switch frames if the hero isn't moving, it's disorienting
    if not keys[pygame.K_RIGHT] and not keys[pygame.K_LEFT] and not keys[pygame.K_DOWN] and not keys[pygame.K_UP]:
        hero.imgSwitch[2] = 0
        
    #if the hero is moving in a blocked direction, stop them at the barrier
    while dirBlocked[1] > (hero.rect.top - 20):
        blockMove = dirBlocked[1] - (hero.rect.top - 20)
        for i in range(numTiles):
            tiles[i].rect.move_ip(0, -blockMove)
        dirBlocked[1] -= blockMove
        needBlit = True
        
    while dirBlocked[3] < (hero.rect.top + hero.rect.height + 20):
        blockMove = dirBlocked[3] - (hero.rect.top + hero.rect.height + 20)
        for i in range(numTiles):
            tiles[i].rect.move_ip(0, -blockMove)
        dirBlocked[3] += -blockMove
        needBlit = True
        
    while dirBlocked[0] > (hero.rect.left - 20):
        blockMove = dirBlocked[0] - (hero.rect.left - 20)
        for i in range(numTiles):
            tiles[i].rect.move_ip(-blockMove, 0)
        dirBlocked[0] -= blockMove
        needBlit = True
        
    while dirBlocked[2] < (hero.rect.left + hero.rect.width + 20):
        blockMove = dirBlocked[2] - (hero.rect.left + hero.rect.width + 20)
        for i in range(numTiles):
            tiles[i].rect.move_ip(-blockMove, 0)
        dirBlocked[2] += -blockMove
        needBlit = True
         
    #evaluate which tiles are on screen, so we only blit them
    screenTiles[0] = 0    
    for i in range(numTiles):
        if (tiles[i].rect.left > (0 - (tiles[i].rect.width + 1)) and tiles[i].rect.left < width
            and tiles[i].rect.top > (0 - (tiles[i].rect.height + 1)) and tiles[i].rect.top < height):
            screenTiles[0] += 1
            screenTiles[screenTiles[0]] = i
    #setImage returns True if the character leveled up, so we can reset the screen
    if hero.setImage():
        needBlit = True
    bossDX = boss.rect.left - tiles[bossTile].rect.left
    bossDY = boss.rect.top - tiles[bossTile].rect.top
    boss.rect = pygame.Rect(tiles[bossTile].rect.left, tiles[bossTile].rect.top, boss.rect.width, boss.rect.height)
    #if we need to blit all the tiles on screen, do so
    if needBlit:
        screen.fill(black)
        for i in range(screenTiles[0]):
            screen.blit(tiles[screenTiles[i + 1]].img, tiles[screenTiles[i + 1]].rect)
    #otherwise, only blit the tiles the character is on, to keep up with the character animation
    else:
        for i in range(currentTiles[0]):
            screen.blit(tiles[currentTiles[i + 1]].img, tiles[currentTiles[i + 1]].rect)
    #print(hero.imgSwitch)
    screen.blit(boss.img, boss.rect)
    screen.blit(hero.img, hero.rect)
    pygame.display.flip()
    #if we step onto a tile with combat, launch the combat and then turn off combat for that tile
    #alreadyBattled is to prevent immediate back-to-back battles
    alreadyBattled = False
    for i in range(currentTiles[0]):
        if tiles[currentTiles[i + 1]].battle:
            if not alreadyBattled:
                battle()
                #set the screen back to the dungeon after the battle
                screen.fill(black)
                for j in range(screenTiles[0]):
                    screen.blit(tiles[screenTiles[j + 1]].img, tiles[screenTiles[j + 1]].rect)
                screen.blit(hero.img, hero.rect)
                pygame.display.flip()
            alreadyBattled = True
            tiles[currentTiles[i + 1]].battle = False
    #if the player steps onto the boss, launch boss fight and then create new dungeon
    if hero.rect.colliderect(boss.rect):
        battle(3)
        newDungeon()